# Loan submission service #

This README would normally document whatever steps are necessary to get your application up and running.

### Project description ###

* Homework for TWINO.
* Loan submission and listing service.
* Requirements link: https://github.com/yurachud/homework/blob/master/README.md

### Project setup and build ###

Project uses Gradle build system.

Project is built using Spring framework with Spring Boot tool. The application contains embedded Tomcat container and H2 database, so no additional setup is needed.

**Note 1:** If You do not have Gradle Build Tool v3.3 or higher installed in Your system, then run Gradle tasks using Gradle Wrapper by executing *gradlew [task_name]*. In UNIX environment You may have to give executable permission to *gradlew* script file and run it with *sudo*.

**Note 2:** Make sure You have enabled annotation processing in Your IDE, because project is using Lombok to automatically add getters/setters, equals(), hashCode() and toString() methods.

**Note 3** Java SE Development Kit 8 is required to build the project. Version 1.8.0_102 (8u102) was used to build this project.

You can use an IDE that supports importing projects from VCS and Gradle, or You can manually run Gradle tasks from a shell. In the second case follow these two steps:

1. Clone the repository using Git.
2. In project root directory execute *gradle build*. Building can take 25-30 seconds or more because of delayed tests. The executable *loan-app-1.0.jar* can be found in *project_root/build/libs/* directory.

### Running the application ###

**The easy way:** to run the application using these default settings:

* *test* mode with in-memory H2 database
* 1 loan submission request per second from single country

... go to *project_root/build/libs/* and execute *java -jar loan-app-1.0.jar*. The server will start up and You should see a message *Started LoanApplication in 11.558 seconds* (time amount can be different) on success.

**Run with modified settings:** 

The application has two modes, each one has effect on embedded H2 database:

* *test* mode (default): in-memory embedded H2 database is available. After application stops, data and database itself are destroyed.
* *production* mode: file-based H2 database is available. After application stops, data exists in an H2 file and can be used with next application run.

To run in production mode, create a file *application.properties* in the same directory where *loan-app-1.0.jar* is located. The file must contain this line: 

    spring.profiles.active=prod 

(default value is *test*). After application has started, database data file *prod.mv.db* and trace file *prod.trace.db* should be created in a directory where application JAR is located.

-------

The application has always-on request rate filter (request count per timeframe for each country). Default settings are 1 request per 1 second.

To change request rate filter settings, create file */config/user_config.properties* relative to executable JAR. The file must contain two lines:

timeframe in milliseconds, valid interval is from *0* to *9_223_372_036_854_775_807*

    request.rate.interval=1000

request count, valid values are from *1* to *2_147_483_647*

    request.rate.count=1

### Using the application ###

After application has started, follow this URL: http://localhost:8080/swagger-ui.html to see Swagger UI documentation of loan service endpoints. You can use it to perform requests.

**Note:** some validation logic is implemented for loan submission. It was not mentioned in requirements but was implemented because data validity is too important to ignore it. Validation rules are:

* *name* and *surname* must not be null and must not contain digits
* *personalId* must not be null
* *term* must not be null and must be between today and 2 years from today
* *amount* must not be null and must be between 0 and 10000

To see contents of a database, follow http://localhost:8080/console to get H2 console. Then:

* if application is started in test mode (in-memory database):
     * JDBC URL: *jdbc:h2:mem:test*
     * User Name: *admin*
     * Password: *twinosecret*
* if application is started in production mode (file-based database):
     * JDBC URL: *jdbc:h2:./prod*
     * User Name: *admin*
     * Password: *twinosecret*

To manage personal id blacklist, You should manually insert it into a database using H2 console. **There is no RESTful API for this purpose,** because it would clash with requirements (such functionality was not mentioned, so not expected to be implemented).

To add personal ID *SOME-ID-012345*, execute:

    INSERT INTO PERSONAL_ID_BLACKLIST VALUES ('SOME-ID-012345'); 

To list all blacklisted personal IDs, execute:

    SELECT * FROM PERSONAL_ID_BLACKLIST;

To remove personal ID *SOME-ID-012345*, execute:

    DELETE FROM PERSONAL_ID_BLACKLIST WHERE ID = 'SOME-ID-012345';

To remove all personal IDs, execute:

    DELETE FROM PERSONAL_ID_BLACKLIST;