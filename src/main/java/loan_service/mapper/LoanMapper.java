package loan_service.mapper;

import loan_service.dto.LoanDto;
import loan_service.model.entity.Loan;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class LoanMapper
{
    private final UserMapper userMapper = new UserMapper();

    public LoanDto mapLoanToLoanDto(Loan loan)
    {
        return new LoanDto(loan.getId(),
                           loan.getAmount(),
                           loan.getTerm(),
                           loan.getCountryCode(),
                           loan.getUser() == null ? null : userMapper.mapUserToUserDto(loan.getUser()));
    }

    public List<LoanDto> mapLoanListToLoanDtoList(List<Loan> loans)
    {
        return loans
                .stream()
                .filter(loan -> loan != null)
                .map(this::mapLoanToLoanDto)
                .collect(Collectors.toList());
    }
}
