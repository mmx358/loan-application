package loan_service.mapper;

import loan_service.dto.UserDto;
import loan_service.model.entity.User;
import org.springframework.stereotype.Component;

@Component
public class UserMapper
{
    public UserDto mapUserToUserDto(User user)
    {
        return new UserDto(user.getId(),
                           user.getName(),
                           user.getSurname(),
                           user.getPersonalId());
    }
}
