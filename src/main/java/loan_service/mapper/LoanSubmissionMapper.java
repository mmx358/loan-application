package loan_service.mapper;

import loan_service.dto.LoanDto;
import loan_service.dto.LoanSubmissionDto;
import loan_service.dto.UserDto;
import loan_service.model.entity.Loan;
import loan_service.model.entity.User;
import org.springframework.stereotype.Component;

@Component
public class LoanSubmissionMapper
{
    public Loan mapLoanSubmissionDtoToLoan(LoanSubmissionDto loanSubmissionDto)
    {
        return new Loan(loanSubmissionDto.getAmount(),
                        loanSubmissionDto.getTerm(),
                        loanSubmissionDto.getCountryCode());
    }

    public User mapLoanApplicationDtoToUser(LoanSubmissionDto loanSubmissionDto)
    {
        return new User(loanSubmissionDto.getName(),
                        loanSubmissionDto.getSurname(),
                        loanSubmissionDto.getPersonalId());
    }

    public LoanDto mapLoanApplicationDtoToLoanDto(LoanSubmissionDto loanSubmissionDto)
    {
        return new LoanDto(null,
                           loanSubmissionDto.getAmount(),
                           loanSubmissionDto.getTerm(),
                           loanSubmissionDto.getCountryCode(),
                           new UserDto(null,
                                       loanSubmissionDto.getName(),
                                       loanSubmissionDto.getSurname(),
                                       loanSubmissionDto.getPersonalId()));
    }
}
