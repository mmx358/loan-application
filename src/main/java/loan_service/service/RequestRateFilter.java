package loan_service.service;

import loan_service.exception.TooManyRequestsException;
import org.apache.commons.collections.buffer.CircularFifoBuffer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

import static org.apache.commons.lang3.StringUtils.isBlank;

@Scope("singleton")
@Service
public class RequestRateFilter
{
    @Value("${request.rate.interval}")
    private long interval;

    @Value("${request.rate.count}")
    private int requestCount;

    private final Map<String, CircularFifoBuffer> state;

    public RequestRateFilter()
    {
        state = new HashMap<>();
    }

    public void filter(String countryCode, Long createdOnMillis)
            throws TooManyRequestsException, IllegalArgumentException
    {
        if (isBlank(countryCode) || createdOnMillis == null)
            throw new IllegalArgumentException();

        synchronized (this) {
            state.putIfAbsent(countryCode, new CircularFifoBuffer(requestCount));
            CircularFifoBuffer requests = state.get(countryCode);
            if (requests.isFull() && ((long) requests.get() > createdOnMillis - interval)) {
                requests.add(createdOnMillis);
                throw new TooManyRequestsException();
            } else {
                requests.add(createdOnMillis);
            }
        }
    }
}
