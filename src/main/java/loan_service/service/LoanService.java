package loan_service.service;

import loan_service.dto.LoanDto;
import loan_service.dto.LoanSubmissionDto;
import loan_service.exception.BlacklistedPersonalIdException;
import loan_service.exception.InvalidUserFilterException;
import loan_service.exception.validation.InvalidLoanSubmissionException;
import loan_service.exception.validation.UserDataMismatchException;
import loan_service.mapper.LoanMapper;
import loan_service.mapper.LoanSubmissionMapper;
import loan_service.model.entity.Loan;
import loan_service.model.entity.User;
import loan_service.model.repository.LoanRepository;
import loan_service.model.repository.PersonalIdBlacklistRepository;
import loan_service.model.repository.UserRepository;
import loan_service.service.validator.LoanSubmissionDtoValidator;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

import static org.apache.commons.lang3.StringUtils.isNotBlank;

@Service
public class LoanService
{
    private final Logger LOGGER = Logger.getLogger(LoanService.class);

    private final LoanMapper loanMapper;
    private final LoanSubmissionMapper loanSubmissionMapper;
    private final LoanSubmissionDtoValidator validator;
    private final PersonalIdBlacklistRepository blacklistRepository;
    private final UserRepository userRepository;
    private final LoanRepository loanRepository;
    private final UserService userService;

    @Autowired
    public LoanService(LoanMapper loanMapper,
                       LoanSubmissionMapper mapper,
                       LoanSubmissionDtoValidator validator,
                       PersonalIdBlacklistRepository blacklistRepository,
                       UserRepository userRepository,
                       LoanRepository loanRepository,
                       UserService userService)
    {
        this.loanMapper = loanMapper;
        this.loanSubmissionMapper = mapper;
        this.validator = validator;
        this.blacklistRepository = blacklistRepository;
        this.userRepository = userRepository;
        this.loanRepository = loanRepository;
        this.userService = userService;
    }

    public LoanDto saveLoan(LoanSubmissionDto loanSubmission)
            throws InvalidLoanSubmissionException, BlacklistedPersonalIdException, UserDataMismatchException
    {
        validator.validate(loanSubmission);

        if (blacklistRepository.exists(loanSubmission.getPersonalId()))
            throw new BlacklistedPersonalIdException();

        User user = userService.createOrGet(loanSubmissionMapper.mapLoanApplicationDtoToUser(loanSubmission));

        Loan loan = loanSubmissionMapper.mapLoanSubmissionDtoToLoan(loanSubmission);
        loan.setUser(user);
        loanRepository.save(loan);

        return loanMapper.mapLoanToLoanDto(loan);
    }

    public List<LoanDto> findAllLoansByUser(String personalId) throws InvalidUserFilterException
    {
        if (personalId == null) {
            return loanMapper.mapLoanListToLoanDtoList(loanRepository.findAll());
        } else if (isNotBlank(personalId)) {
            List<LoanDto> foundLoans = new LinkedList<>();
            userRepository.findOneByPersonalId(personalId)
                    .ifPresent(user -> foundLoans.addAll(loanMapper.mapLoanListToLoanDtoList(user.getLoans())));
            return foundLoans;
        } else {
            InvalidUserFilterException e = new InvalidUserFilterException();
            LOGGER.error("Personal ID parameter value is not specified", e);
            throw e;
        }
    }
}
