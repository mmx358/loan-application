package loan_service.service.validator;

import loan_service.dto.LoanSubmissionDto;
import loan_service.exception.validation.InvalidLoanSubmissionException;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

import static java.time.LocalDate.now;
import static org.apache.commons.lang3.StringUtils.isEmpty;

@Component
public class LoanSubmissionDtoValidator
{
    public void validate(LoanSubmissionDto loan) throws InvalidLoanSubmissionException
    {
        final LocalDate today = now();
        final List<String> messages = new LinkedList<>();

        if (loan.getAmount() == null || loan.getAmount() < 0 || loan.getAmount() > 10000)
            messages.add("loan.amount.invalid");

        if (loan.getTerm() == null || loan.getTerm().isBefore(today) || loan.getTerm().isAfter(today.plusMonths(24)))
            messages.add("loan.term.invalid");

        if (isEmpty(loan.getName()) || loan.getName().matches(".*\\d+.*"))
            messages.add("loan.name.invalid");

        if (isEmpty(loan.getSurname()) || loan.getSurname().matches(".*\\d+.*"))
            messages.add("loan.surname.invalid");

        if (isEmpty(loan.getPersonalId()))
            messages.add("loan.personalId.invalid");

        if (isEmpty(loan.getCountryCode()) || loan.getCountryCode().length() > 3)
            messages.add("loan.countryCode.invalid");

        if (!messages.isEmpty())
            throw new InvalidLoanSubmissionException(messages);
    }
}
