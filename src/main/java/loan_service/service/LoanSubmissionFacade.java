package loan_service.service;

import loan_service.dto.LoanDto;
import loan_service.dto.LoanSubmissionDto;
import loan_service.exception.BlacklistedPersonalIdException;
import loan_service.exception.TooManyRequestsException;
import loan_service.exception.UnknownIpException;
import loan_service.exception.validation.InvalidLoanSubmissionException;
import loan_service.exception.validation.UserDataMismatchException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;

import static java.lang.System.currentTimeMillis;

@Service
public class LoanSubmissionFacade
{
    private final Logger LOGGER = Logger.getLogger(LoanSubmissionFacade.class);

    private final IPResolutionService ipResolutionService;
    private final RequestRateFilter requestRateFilter;
    private final LoanService loanService;

    @Autowired
    public LoanSubmissionFacade(IPResolutionService ipResolutionService,
                                LoanService loanService,
                                RequestRateFilter requestRateFilter)
    {
        this.ipResolutionService = ipResolutionService;
        this.loanService = loanService;
        this.requestRateFilter = requestRateFilter;
    }

    public LoanDto submit(LoanSubmissionDto loanSubmission, HttpServletRequest httpServletRequest)
    {
        try {
            Long createdOn = currentTimeMillis();
            String countryCode = ipResolutionService.getCountryCode(httpServletRequest);
            requestRateFilter.filter(countryCode, createdOn);
            loanSubmission.setCountryCode(countryCode);
            return loanService.saveLoan(loanSubmission);
        } catch (UnknownIpException e) {
            LOGGER.error("Cannot extract ip address from current request", e);
            throw e;
        } catch (TooManyRequestsException e) {
            LOGGER.error("Too many requests per timeframe from a single country", e);
            throw e;
        } catch (InvalidLoanSubmissionException e) {
            LOGGER.error("Loan submission contains invalid data", e);
            throw e;
        } catch (BlacklistedPersonalIdException e) {
            LOGGER.error("Person ID is in blacklist", e);
            throw e;
        } catch (UserDataMismatchException e) {
            LOGGER.error("User with same personal id is found, but other data does not match", e);
            throw e;
        }
    }
}
