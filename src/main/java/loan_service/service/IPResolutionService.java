package loan_service.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import loan_service.exception.UnknownIpException;
import org.apache.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

@Service
public class IPResolutionService
{
    private final Logger LOGGER = Logger.getLogger(IPResolutionService.class);

    private final RestTemplate restTemplate;

    /*
     * Non-default constructors are workaround to inject RestTemplate as a bean for testing purposes
     */

    public IPResolutionService()
    {
        this.restTemplate = restTemplate();
    }

    public IPResolutionService(RestTemplate restTemplate)
    {
        this.restTemplate = restTemplate;
    }

    @Bean
    public RestTemplate restTemplate()
    {
        return new RestTemplate();
    }

    public String getCountryCode(HttpServletRequest httpServletRequest) throws UnknownIpException
    {
        String ip = httpServletRequest.getRemoteAddr();

        if (isBlank(ip))
            throw new UnknownIpException();

        ResponseEntity<String> response = restTemplate.getForEntity("http://ip-api.com/json" + "/" + ip, String.class);

        String countryCode = null;
        if (HttpStatus.OK.equals(response.getStatusCode())) {
            try {
                countryCode = new ObjectMapper()
                        .readTree(response.getBody())
                        .path("countryCode")
                        .asText();
            } catch (IOException e) {
                LOGGER.error("Cannot read country code from JSON!", e);
            }
        }

        return isNotBlank(countryCode) ? countryCode.toLowerCase() : "lv";
    }
}
