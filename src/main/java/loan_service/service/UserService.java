package loan_service.service;

import loan_service.exception.validation.UserDataMismatchException;
import loan_service.model.entity.User;
import loan_service.model.repository.UserRepository;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserService
{
    private final UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository)
    {
        this.userRepository = userRepository;
    }

    public User createOrGet(User user) throws UserDataMismatchException
    {
        Optional<User> optFoundUser = userRepository.findOneByPersonalId(user.getPersonalId());

        if (optFoundUser.isPresent()) {
            User foundUser = optFoundUser.get();
            if (foundUser.getName().equals(user.getName()) && foundUser.getSurname().equals(user.getSurname())) {
                return foundUser;
            } else {
                throw new UserDataMismatchException();
            }
        } else {
            userRepository.save(user);
            return user;
        }
    }
}
