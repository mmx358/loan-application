package loan_service.controller;

import io.swagger.annotations.*;
import loan_service.dto.LoanDto;
import loan_service.dto.LoanSubmissionDto;
import loan_service.service.LoanService;
import loan_service.service.LoanSubmissionFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Api(value = "/loan", description = "Loan operations")
@RestController
@RequestMapping("/loan")
public class LoanController
{
    private final LoanService loanService;
    private final LoanSubmissionFacade loanSubmissionFacade;

    @Autowired
    public LoanController(LoanService loanService,
                          LoanSubmissionFacade loanSubmissionFacade)
    {
        this.loanService = loanService;
        this.loanSubmissionFacade = loanSubmissionFacade;
    }

    @ApiOperation(value = "Find all loans",
            notes = "Personal ID can be provided as filtering parameter",
            response = LoanDto.class,
            responseContainer = "List")
    @ApiResponses({
            @ApiResponse(code = 422, message = "Personal ID value is not specified")
    })
    @GetMapping
    public List<LoanDto> getLoans(
            @ApiParam(value = "Personal ID to find loans for")
            @RequestParam(value = "personalId", required = false)
                    String personalId)
    {
        return loanService.findAllLoansByUser(personalId);
    }

    @ApiOperation(value = "Create loan",
            notes = "All fields in request body object must be provided. \"term\" must be between today to +2 years from today. \"amount\" must be between 0 to 10000.",
            response = LoanDto.class)
    @ApiResponses({
            @ApiResponse(code = 400, message = "Cannot extract IP address from request"),
            @ApiResponse(code = 403, message = "Personal ID is in blacklist"),
            @ApiResponse(code = 409, message = "Submitted user name and surname do not match with existing user with the same personal ID"),
            @ApiResponse(code = 422, message = "Submitted data contains is not valid"),
            @ApiResponse(code = 429, message = "Too many requests per time interval for a given country")
    })
    @PostMapping
    public LoanDto postLoan(
            @ApiParam(value = "Loan submission object")
            @RequestBody
                    LoanSubmissionDto loanSubmissionDto,
            HttpServletRequest request)
    {
        return loanSubmissionFacade.submit(loanSubmissionDto, request);
    }
}
