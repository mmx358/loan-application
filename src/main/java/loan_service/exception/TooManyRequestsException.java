package loan_service.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.TOO_MANY_REQUESTS, reason = "too.many.requests.from.same.country")
public class TooManyRequestsException extends LoanApplicationException
{
}
