package loan_service.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "remote.address.not.preset")
public class UnknownIpException extends LoanApplicationException
{
}
