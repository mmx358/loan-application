package loan_service.exception.validation;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.List;

@ResponseStatus(code = HttpStatus.UNPROCESSABLE_ENTITY, reason = "loan.submission.invalid")
public class InvalidLoanSubmissionException extends ValidationException
{
    public InvalidLoanSubmissionException()
    {
    }

    public InvalidLoanSubmissionException(List<String> messages)
    {
        super(messages);
    }
}
