package loan_service.exception.validation;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.CONFLICT, reason = "user.data.mismatch")
public class UserDataMismatchException extends ValidationException
{
}
