package loan_service.exception.validation;

import loan_service.exception.LoanApplicationException;

import java.util.Collections;
import java.util.List;

public class ValidationException extends LoanApplicationException
{
    private final List<String> messages;

    public ValidationException()
    {
        this.messages = Collections.emptyList();
    }

    public ValidationException(List<String> messages)
    {
        this.messages = Collections.unmodifiableList(messages);
    }

    public List<String> getMessages()
    {
        return this.messages;
    }
}
