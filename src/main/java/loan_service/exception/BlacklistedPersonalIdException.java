package loan_service.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.FORBIDDEN, reason = "personalId.in.blacklist")
public class BlacklistedPersonalIdException extends LoanApplicationException
{
}
