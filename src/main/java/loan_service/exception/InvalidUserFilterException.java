package loan_service.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.UNPROCESSABLE_ENTITY, reason = "user.filter.invalid")
public class InvalidUserFilterException extends LoanApplicationException
{
}
