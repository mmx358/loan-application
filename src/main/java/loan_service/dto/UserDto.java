package loan_service.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class UserDto
{
    private Long id;
    private String name;
    private String surname;
    private String personalId;
}
