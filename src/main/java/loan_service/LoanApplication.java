package loan_service;

import org.h2.server.web.WebServlet;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;

@PropertySource("classpath:./config/user_config.properties")
@PropertySource(value = "file:./config/user_config.properties", ignoreResourceNotFound = true)
@SpringBootApplication
public class LoanApplication
{
    public static void main(String[] args)
    {
        SpringApplication.run(LoanApplication.class, args);
    }
}
