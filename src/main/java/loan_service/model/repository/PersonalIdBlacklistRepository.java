package loan_service.model.repository;

import loan_service.model.entity.PersonalId;
import org.springframework.stereotype.Repository;

@Repository
public interface PersonalIdBlacklistRepository extends BaseRepository<PersonalId, String>
{
}
