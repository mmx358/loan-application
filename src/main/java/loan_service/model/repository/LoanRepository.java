package loan_service.model.repository;

import loan_service.model.entity.Loan;

public interface LoanRepository extends BaseRepository<Loan, Long>
{
}
