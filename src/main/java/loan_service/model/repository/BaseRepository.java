package loan_service.model.repository;

import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.Repository;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;

@NoRepositoryBean
public interface BaseRepository<T, ID extends Serializable> extends Repository<T, ID>
{
    <S extends T> Optional<S> save(S entity);

    <S extends T> List<S> save(Iterable<S> entities);

    Optional<T> findOne(ID id);

    List<T> findAll();

    boolean exists(ID id);

    void delete(ID id);
}
