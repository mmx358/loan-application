package loan_service.model.repository;

import loan_service.model.entity.User;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends BaseRepository<User, Long>
{
    Optional<User> findOneByPersonalId(String personalId);
}
