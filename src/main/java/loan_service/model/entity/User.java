package loan_service.model.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.LinkedList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class User
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    private String name;

    @NotNull
    private String surname;

    @NotNull
    private String personalId;

    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
    private List<Loan> loans;

    public User(String name, String surname, String personalId)
    {
        this(name, surname, personalId, new LinkedList<>());
    }

    public User(String name, String surname, String personalId, List<Loan> loans)
    {
        this.name = name;
        this.surname = surname;
        this.personalId = personalId;
        this.loans = loans;
    }
}
