package loan_service.model.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Loan
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    private Double amount;

    @NotNull
    private LocalDate term;

    @NotNull
    private String countryCode;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private User user;

    public Loan(Double amount, LocalDate term, String countryCode)
    {
        this(amount, term, countryCode, null);
    }

    public Loan(Double amount, LocalDate term, String countryCode, User user)
    {
        this.amount = amount;
        this.term = term;
        this.countryCode = countryCode;
        this.user = user;
    }
}
