--liquibase formatted sql

--changeset nvoxland:1
CREATE TABLE user (
  id          IDENTITY,
  name        VARCHAR(80) NOT NULL,
  surname     VARCHAR(80) NOT NULL,
  personal_id VARCHAR(80) NOT NULL UNIQUE
);

--changeset nvoxland:2
CREATE TABLE loan (
  id           IDENTITY,
  amount       DECIMAL    NOT NULL,
  term         DATE       NOT NULL,
  country_code VARCHAR(3) NOT NULL,
  user_id      NUMERIC    NOT NULL,
  FOREIGN KEY (user_id) REFERENCES user (id)
);

--changeset nvoxland:3
CREATE TABLE personal_id_blacklist (
  id VARCHAR(80) PRIMARY KEY
);