package loan_service.model.repository;

import loan_service.factory.LoanFactoryForTests;
import loan_service.factory.UserFactoryForTests;
import loan_service.model.entity.Loan;
import loan_service.model.entity.User;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.NoSuchElementException;

import static org.unitils.reflectionassert.ReflectionAssert.assertReflectionEquals;

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class UserWithLoansPersistenceTest
{
    @Autowired
    private UserRepository userRepository;

    private User user;
    private List<Loan> loans;

    @Before
    public void setUp() throws Exception
    {
        loans = LoanFactoryForTests.getListOfThree();
        loans.forEach(loan -> loan.setId(null));

        user = UserFactoryForTests.getInstance(loans);
        user.setId(null);
    }

    @Test
    public void saveUserWithLoans_LoansExist_LoansAreLoaded() throws Exception
    {
        userRepository.save(user);

        User foundUser = userRepository.findOne(user.getId())
                .orElseThrow(() -> new NoSuchElementException(
                        "User with id = " + user.getId() + " was not found."));

        assertReflectionEquals(user, foundUser);
    }
}
