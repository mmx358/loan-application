package loan_service.model.repository;

import loan_service.model.entity.Loan;
import loan_service.model.entity.User;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.NoSuchElementException;

import static loan_service.factory.LoanFactoryForTests.getListOfThree;
import static loan_service.factory.UserFactoryForTests.getInstance;
import static org.unitils.reflectionassert.ReflectionAssert.assertReflectionEquals;

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class LoanRepositoryTest
{
    @Autowired
    private LoanRepository loanRepository;

    @Autowired
    private UserRepository userRepository;

    private List<Loan> loans;

    @Before
    public void setUp() throws Exception
    {
        User user = getInstance();
        user.setId(null);
        userRepository.save(user);

        loans = getListOfThree();
        loans.forEach(loan -> loan.setUser(user));

        loanRepository.save(loans);
    }

    @Test
    public void findOneById_Exists_EntityReturned() throws Exception
    {
        for (Loan savedLoan : loans) {

            Loan foundLoan = loanRepository.findOne(savedLoan.getId())
                    .orElseThrow(() -> new NoSuchElementException(
                            "Loan with id = " + savedLoan.getId() + " was not found."));

            assertReflectionEquals(savedLoan, foundLoan);
        }
    }
}