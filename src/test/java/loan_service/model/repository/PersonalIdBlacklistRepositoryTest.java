package loan_service.model.repository;

import loan_service.model.entity.PersonalId;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.NoSuchElementException;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.unitils.reflectionassert.ReflectionAssert.assertReflectionEquals;

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class PersonalIdBlacklistRepositoryTest
{
    @Autowired
    private PersonalIdBlacklistRepository repository;

    private PersonalId personalId = new PersonalId("ABC-456");

    @Test
    public void findOne_PersonalIdExists_EntityReturned() throws Exception
    {
        repository.save(personalId);

        PersonalId foundPersonalId = repository.findOne(personalId.getId())
                .orElseThrow(() -> new NoSuchElementException(
                        "Personal id with id = " + personalId.getId() + " was not found."));

        assertReflectionEquals(personalId, foundPersonalId);
    }

    @Test
    public void exists_PersonIdNotExists_ReturnFalse() throws Exception
    {
        assertFalse(repository.exists(personalId.getId()));
    }

    @Test
    public void exists_PersonIdExists_ReturnTrue() throws Exception
    {
        repository.save(personalId);

        assertTrue(repository.exists(personalId.getId()));
    }
}
