package loan_service.model.repository;

import loan_service.model.entity.User;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.NoSuchElementException;

import static loan_service.factory.UserFactoryForTests.getListOfThree;
import static org.unitils.reflectionassert.ReflectionAssert.assertReflectionEquals;

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class UserRepositoryTest
{
    @Autowired
    private UserRepository repository;

    private List<User> users;

    @Before
    public void setUp()
    {
        users = getListOfThree();
        repository.save(users);
    }

    @Test
    public void findOneById_Exists_EntityReturned() throws Exception
    {
        for (User savedUser : users) {

            User foundUser = repository.findOne(savedUser.getId())
                    .orElseThrow(() -> new NoSuchElementException(
                            "User with id = " + savedUser.getId() + " was not found."));

            assertReflectionEquals(savedUser, foundUser);
        }
    }

    @Test
    public void findOneByPersonalId_Exists_EntityReturned() throws Exception
    {
        for (User savedUser : users) {

            User foundUser = repository.findOneByPersonalId(savedUser.getPersonalId())
                    .orElseThrow(() -> new NoSuchElementException(
                            "User with id = " + savedUser.getId() + " was not found."));

            assertReflectionEquals(savedUser, foundUser);
        }
    }
}