package loan_service.model.repository;

import loan_service.factory.LoanFactoryForTests;
import loan_service.factory.UserFactoryForTests;
import loan_service.model.entity.Loan;
import loan_service.model.entity.User;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.NoSuchElementException;

import static org.unitils.reflectionassert.ReflectionAssert.assertReflectionEquals;

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class LoanWithUserPersistenceTest
{
    @Autowired
    private LoanRepository loanRepository;

    @Autowired
    private UserRepository userRepository;

    private Loan loan;

    private User user;

    @Before
    public void setUp() throws Exception
    {
        user = UserFactoryForTests.getInstance();
        user.setId(null);

        loan = LoanFactoryForTests.getInstance(user);
        loan.setId(null);
    }

    @Test(expected = DataIntegrityViolationException.class)
    public void saveLoanWithUser_UserNotExist_ExceptionThrown() throws Exception
    {
        loanRepository.save(loan);
    }

    @Test
    public void saveLoanWithUser_UserExists_RelationshipCreated() throws Exception
    {
        userRepository.save(user);

        loanRepository.save(loan);

        Loan foundLoan = loanRepository.findOne(loan.getId())
                .orElseThrow(() -> new NoSuchElementException(
                        "Loan with id = " + loan.getId() + " was not found."));

        assertReflectionEquals(loan, foundLoan);
    }
}
