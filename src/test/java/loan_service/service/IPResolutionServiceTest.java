package loan_service.service;

import loan_service.exception.UnknownIpException;
import loan_service.test_utils.FileLoader;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;

import static org.apache.commons.lang3.StringUtils.isEmpty;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
public class IPResolutionServiceTest
{
    @Mock
    private RestTemplate restTemplate;

    @InjectMocks
    private IPResolutionService service;

    private HttpServletRequest servletRequestWithoutAddress;
    private HttpServletRequest servletRequestWithAddress;

    @Before
    public void setUp() throws Exception
    {
        servletRequestWithoutAddress = Mockito.mock(HttpServletRequest.class);
        when(servletRequestWithoutAddress.getRemoteAddr())
                .thenReturn(null);

        servletRequestWithAddress = Mockito.mock(HttpServletRequest.class);
        when(servletRequestWithAddress.getRemoteAddr())
                .thenReturn("208.80.152.201");
    }

    @Test(expected = UnknownIpException.class)
    public void getCountry_RemoteAddressIsNull_ExceptionThrown() throws Exception
    {
        service.getCountryCode(servletRequestWithoutAddress);
    }

    @Test
    public void getCountry_ResponseNotOk_ExceptionThrown() throws Exception
    {
        ResponseEntity<String> response = new ResponseEntity<>(HttpStatus.NOT_FOUND);

        when(restTemplate.getForEntity(anyString(), eq(String.class)))
                .thenReturn(response);

        String countryCode = service.getCountryCode(servletRequestWithAddress);

        assertEquals("lv", countryCode);
    }

    @Test
    public void getCountry_BodyNotPresent_CodeReturned() throws Exception
    {
        String body = "";

        ResponseEntity<String> response = new ResponseEntity<>(body, HttpStatus.OK);

        when(restTemplate.getForEntity(anyString(), eq(String.class)))
                .thenReturn(response);

        String countryCode = service.getCountryCode(servletRequestWithAddress);

        assertEquals("lv", countryCode);
    }

    @Test
    public void getCountry_ResponseOk_CodeReturned() throws Exception
    {
        String body = new FileLoader().getFileAsString("files/ip_response.json");
        if (isEmpty(body))
            fail();

        ResponseEntity<String> response = new ResponseEntity<>(body, HttpStatus.OK);

        when(restTemplate.getForEntity(anyString(), eq(String.class)))
                .thenReturn(response);

        String countryCode = service.getCountryCode(servletRequestWithAddress);

        assertEquals("us", countryCode);
    }
}