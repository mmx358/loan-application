package loan_service.service;

import loan_service.exception.validation.UserDataMismatchException;
import loan_service.model.entity.User;
import loan_service.model.repository.UserRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static loan_service.factory.UserFactoryForTests.getInstance;
import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;
import static org.unitils.reflectionassert.ReflectionAssert.assertReflectionEquals;

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
public class UserServiceTest
{
    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private UserService userService;

    @Test
    public void createOrGet_UserNotExistsAndSaved_UserReturned() throws Exception
    {
        User user = getInstance();
        user.setId(null);

        User savedUser = getInstance();

        when(userRepository.findOneByPersonalId(anyString()))
                .thenReturn(Optional.empty());

        when(userRepository.save(any(User.class)))
                .then(new Answer<Optional<User>>()
                {
                    @Override
                    public Optional<User> answer(InvocationOnMock invocation) throws Throwable
                    {
                        User user = (User) invocation.getArguments()[0];
                        user.setId(savedUser.getId());
                        return Optional.of(user);
                    }
                })
                .thenReturn(Optional.of(savedUser));

        User actualSavedUser = userService.createOrGet(user);

        assertEquals(savedUser.getId(), actualSavedUser.getId());
        assertEquals(savedUser.getName(), actualSavedUser.getName());
        assertEquals(savedUser.getSurname(), actualSavedUser.getSurname());

        verify(userRepository, times(1)).findOneByPersonalId(eq(user.getPersonalId()));
        verify(userRepository, times(1)).save(eq(user));
    }

    @Test(expected = UserDataMismatchException.class)
    public void createOrGet_UserExistsAndNotMatches_ExceptionThrown() throws Exception
    {
        User user = getInstance();
        user.setId(null);

        User existingUser = getInstance();
        existingUser.setName(user.getName() + "_1");
        existingUser.setSurname(user.getSurname() + "_1");

        when(userRepository.findOneByPersonalId(anyString()))
                .thenReturn(Optional.of(existingUser));

        userService.createOrGet(user);

        verify(userRepository, times(1)).findOneByPersonalId(eq(user.getPersonalId()));
        verify(userRepository, never()).save(eq(user));
    }

    @Test
    public void createOrGet_UserExistsAndMatches_UserReturned() throws Exception
    {
        User user = getInstance();
        user.setId(null);

        User existingUser = getInstance();

        when(userRepository.findOneByPersonalId(anyString()))
                .thenReturn(Optional.of(existingUser));

        User returnedExistingUser = userService.createOrGet(user);

        assertReflectionEquals(existingUser, returnedExistingUser);

        verify(userRepository, times(1)).findOneByPersonalId(eq(user.getPersonalId()));
        verify(userRepository, never()).save(eq(user));
    }
}