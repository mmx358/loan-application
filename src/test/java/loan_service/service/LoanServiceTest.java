package loan_service.service;

import loan_service.dto.LoanDto;
import loan_service.dto.LoanSubmissionDto;
import loan_service.exception.BlacklistedPersonalIdException;
import loan_service.exception.validation.InvalidLoanSubmissionException;
import loan_service.factory.LoanApplicationDtoFactoryForTests;
import loan_service.factory.LoanFactoryForTests;
import loan_service.factory.UserFactoryForTests;
import loan_service.mapper.LoanMapper;
import loan_service.mapper.LoanSubmissionMapper;
import loan_service.model.entity.Loan;
import loan_service.model.entity.User;
import loan_service.model.repository.LoanRepository;
import loan_service.model.repository.PersonalIdBlacklistRepository;
import loan_service.model.repository.UserRepository;
import loan_service.service.validator.LoanSubmissionDtoValidator;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Optional;

import static java.util.Collections.emptyList;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;
import static org.unitils.reflectionassert.ReflectionAssert.assertReflectionEquals;

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
public class LoanServiceTest
{
    @Mock
    private LoanMapper loanMapper;

    @Mock
    private LoanSubmissionMapper loanSubmissionMapper;

    @Mock
    private LoanSubmissionDtoValidator validator;

    @Mock
    private PersonalIdBlacklistRepository blacklistRepository;

    @Mock
    private UserRepository userRepository;

    @Mock
    private LoanRepository loanRepository;

    @Mock
    private UserService userService;

    private LoanService loanService;


    private LoanSubmissionDto loanSubmissionDto;
    private User user;
    private Loan loan;

    @Captor
    private ArgumentCaptor<List<Loan>> loanListArgumentCaptor;

    @Before
    public void setUp() throws Exception
    {
        MockitoAnnotations.initMocks(this);

        loanService = new LoanService(loanMapper,
                                      loanSubmissionMapper,
                                      validator,
                                      blacklistRepository,
                                      userRepository,
                                      loanRepository,
                                      userService);

        loanSubmissionDto = LoanApplicationDtoFactoryForTests.getInstance();

        user = new User(loanSubmissionDto.getName(),
                        loanSubmissionDto.getSurname(),
                        loanSubmissionDto.getPersonalId());

        loan = new Loan(loanSubmissionDto.getAmount(),
                        loanSubmissionDto.getTerm(),
                        loanSubmissionDto.getCountryCode());
    }

    @Test(expected = InvalidLoanSubmissionException.class)
    public void saveLoan_ValidationFailed_ExceptionThrown() throws Exception
    {
        Mockito.doThrow(new InvalidLoanSubmissionException())
                .when(validator).validate(eq(loanSubmissionDto));

        loanService.saveLoan(loanSubmissionDto);

        verify(validator, times(1)).validate(eq(loanSubmissionDto));
        verify(blacklistRepository, never()).exists(anyString());
        verify(loanSubmissionMapper, never()).mapLoanApplicationDtoToUser(any(LoanSubmissionDto.class));
        verify(userService, never()).createOrGet(any(User.class));
        verify(loanSubmissionMapper, never()).mapLoanSubmissionDtoToLoan(any(LoanSubmissionDto.class));
        verify(loanRepository, never()).save(any(Loan.class));
        verify(loanMapper, never()).mapLoanToLoanDto(any(Loan.class));
    }

    @Test(expected = BlacklistedPersonalIdException.class)
    public void saveLoan_PersonalIdBlacklisted_ExceptionThrown() throws Exception
    {
        doNothing()
                .when(validator).validate(eq(loanSubmissionDto));

        when(blacklistRepository.exists(anyString()))
                .thenReturn(true);

        loanService.saveLoan(loanSubmissionDto);

        verify(validator, times(1)).validate(eq(loanSubmissionDto));
        verify(blacklistRepository, times(1)).exists(eq(loanSubmissionDto.getPersonalId()));
        verify(loanSubmissionMapper, never()).mapLoanApplicationDtoToUser(any(LoanSubmissionDto.class));
        verify(userService, never()).createOrGet(any(User.class));
        verify(loanSubmissionMapper, never()).mapLoanSubmissionDtoToLoan(any(LoanSubmissionDto.class));
        verify(loanRepository, never()).save(any(Loan.class));
        verify(loanMapper, never()).mapLoanToLoanDto(any(Loan.class));
    }

    @Test(expected = RuntimeException.class)
    public void saveLoan_GetUserExceptionThrown_ExceptionThrown() throws Exception
    {
        doNothing()
                .when(validator).validate(eq(loanSubmissionDto));

        when(blacklistRepository.exists(anyString()))
                .thenReturn(false);

        when(userService.createOrGet(any(User.class)))
                .thenThrow(new RuntimeException());

        loanService.saveLoan(loanSubmissionDto);

        verify(validator, times(1)).validate(eq(loanSubmissionDto));
        verify(blacklistRepository, times(1)).exists(eq(loanSubmissionDto.getPersonalId()));
        verify(loanSubmissionMapper, times(1)).mapLoanApplicationDtoToUser(eq(loanSubmissionDto));
        verify(userService, times(1)).createOrGet(eq(user));
        verify(loanSubmissionMapper, never()).mapLoanSubmissionDtoToLoan(any(LoanSubmissionDto.class));
        verify(loanRepository, never()).save(any(Loan.class));
        verify(loanMapper, never()).mapLoanToLoanDto(any(Loan.class));
    }

    @Test
    public void saveLoan_GetUserSuccess_LoanSaved() throws Exception
    {
        User returnedUser = new User(15L,
                                     user.getName(),
                                     user.getSurname(),
                                     user.getPersonalId(),
                                     emptyList());
        Loan savedLoan = new Loan(115L,
                                  loan.getAmount(),
                                  loan.getTerm(),
                                  loan.getCountryCode(),
                                  returnedUser);
        LoanDto savedLoanDto = new LoanMapper().mapLoanToLoanDto(savedLoan);

        doNothing()
                .when(validator).validate(eq(loanSubmissionDto));

        when(blacklistRepository.exists(anyString()))
                .thenReturn(false);

        when(loanSubmissionMapper.mapLoanApplicationDtoToUser(any(LoanSubmissionDto.class)))
                .thenReturn(user);

        when(userService.createOrGet(any(User.class)))
                .thenReturn(returnedUser);

        when(loanSubmissionMapper.mapLoanSubmissionDtoToLoan(any(LoanSubmissionDto.class)))
                .thenReturn(loan);

        when(loanRepository.save(any(Loan.class)))
                .thenReturn(Optional.of(savedLoan));

        when(loanMapper.mapLoanToLoanDto(any(Loan.class)))
                .thenReturn(savedLoanDto);

        LoanDto actualSavedLoanDto = loanService.saveLoan(loanSubmissionDto);

        assertReflectionEquals(savedLoanDto, actualSavedLoanDto);

        verify(validator, times(1)).validate(eq(loanSubmissionDto));
        verify(blacklistRepository, times(1)).exists(eq(loanSubmissionDto.getPersonalId()));
        verify(loanSubmissionMapper, times(1)).mapLoanApplicationDtoToUser(eq(loanSubmissionDto));

        ArgumentCaptor<User> userArgumentCaptor = ArgumentCaptor.forClass(User.class);
        verify(userService, times(1)).createOrGet(userArgumentCaptor.capture());
        assertReflectionEquals(user, userArgumentCaptor.getValue());

        verify(loanSubmissionMapper, times(1)).mapLoanSubmissionDtoToLoan(eq(loanSubmissionDto));
        verify(loanRepository, times(1)).save(eq(loan));
        verify(loanMapper, times(1)).mapLoanToLoanDto(any(Loan.class));
    }

    @Test
    public void findLoansByUser_PersonalIdIsNullAndLoansNotFound_EmptyListReturned() throws Exception
    {
        List<LoanDto> expectedLoanDtoList = emptyList();

        when(loanRepository.findAll())
                .thenReturn(emptyList());

        when(loanMapper.mapLoanListToLoanDtoList(anyListOf(Loan.class)))
                .thenReturn(expectedLoanDtoList);

        List<LoanDto> foundLoanDtoList = loanService.findAllLoansByUser(null);

        assertReflectionEquals(expectedLoanDtoList, foundLoanDtoList);
    }

    @Test
    public void findLoansByUser_PersonalIdIsNullAndLoansFound_ListReturned() throws Exception
    {
        List<Loan> foundLoans = LoanFactoryForTests.getListOfThree();
        List<LoanDto> expectedLoanDtoList = new LoanMapper().mapLoanListToLoanDtoList(foundLoans);

        when(loanRepository.findAll())
                .thenReturn(foundLoans);

        when(loanMapper.mapLoanListToLoanDtoList(anyListOf(Loan.class)))
                .thenReturn(expectedLoanDtoList);

        List<LoanDto> foundLoanDtoList = loanService.findAllLoansByUser(null);

        verify(loanRepository, times(1)).findAll();

        verify(loanMapper, times(1)).mapLoanListToLoanDtoList(loanListArgumentCaptor.capture());
        assertReflectionEquals(foundLoans, loanListArgumentCaptor.getValue());

        assertReflectionEquals(expectedLoanDtoList, foundLoanDtoList);
    }

    @Test
    public void findLoansByUser_PersonalIdNotNullAndUserNotFound_EmptyListReturned() throws Exception
    {
        String personalId = "ABCD-1234";
        List<Loan> foundLoans = emptyList();
        List<LoanDto> expectedLoanDtoList = emptyList();

        when(userRepository.findOneByPersonalId(eq(personalId)))
                .thenReturn(Optional.empty());

        when(loanMapper.mapLoanListToLoanDtoList(eq(foundLoans)))
                .thenReturn(expectedLoanDtoList);

        List<LoanDto> foundLoanDtoList = loanService.findAllLoansByUser(personalId);

        verify(userRepository, times(1)).findOneByPersonalId(eq(personalId));
        verify(loanMapper, never()).mapLoanListToLoanDtoList(anyListOf(Loan.class));

        assertReflectionEquals(expectedLoanDtoList, foundLoanDtoList);
    }

    @Test
    public void findLoansByUser_PersonalIdNotNullAndUserFoundAndLoansNotFound_EmptyListReturned() throws Exception
    {
        String personalId = "ABCD-1234";
        List<Loan> foundLoans = emptyList();
        User foundUser = UserFactoryForTests.getInstance(foundLoans);
        List<LoanDto> expectedLoanDtoList = emptyList();

        when(userRepository.findOneByPersonalId(eq(personalId)))
                .thenReturn(Optional.ofNullable(foundUser));

        when(loanMapper.mapLoanListToLoanDtoList(anyListOf(Loan.class)))
                .thenReturn(expectedLoanDtoList);

        List<LoanDto> foundLoanDtoList = loanService.findAllLoansByUser(personalId);

        verify(userRepository, times(1)).findOneByPersonalId(eq(personalId));
        verify(loanMapper, times(1)).mapLoanListToLoanDtoList(loanListArgumentCaptor.capture());

        assertReflectionEquals(foundLoans, loanListArgumentCaptor.getValue());
        assertReflectionEquals(expectedLoanDtoList, foundLoanDtoList);
    }

    @Test
    public void findLoansByUser_PersonalIdNotNullAndUserFoundAndLoansFound_ListReturned() throws Exception
    {
        String personalId = "ABCD-1234";
        List<Loan> foundLoans = LoanFactoryForTests.getListOfThree();
        User foundUser = UserFactoryForTests.getInstance(foundLoans);
        List<LoanDto> expectedLoanDtoList = new LoanMapper().mapLoanListToLoanDtoList(foundLoans);

        when(userRepository.findOneByPersonalId(eq(personalId)))
                .thenReturn(Optional.ofNullable(foundUser));

        when(loanMapper.mapLoanListToLoanDtoList(anyListOf(Loan.class)))
                .thenReturn(expectedLoanDtoList);

        List<LoanDto> foundLoanDtoList = loanService.findAllLoansByUser(personalId);

        verify(userRepository, times(1)).findOneByPersonalId(eq(personalId));
        verify(loanMapper, times(1)).mapLoanListToLoanDtoList(loanListArgumentCaptor.capture());

        assertReflectionEquals(foundLoans, loanListArgumentCaptor.getValue());
        assertReflectionEquals(expectedLoanDtoList, foundLoanDtoList);
    }
}