package loan_service.service;

import loan_service.exception.TooManyRequestsException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;

import static java.lang.System.currentTimeMillis;

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
public class RequestRateFilterTest
{
    @InjectMocks
    private RequestRateFilter filter;

    private long interval = 1000L;
    private int requestCount = 1;

    private final String LV = "lv";
    private final String US = "us";
    private final String UK = "uk";

    private final Long now = currentTimeMillis();

    @Before
    public void setUp() throws Exception
    {
        ReflectionTestUtils.setField(filter, "interval", interval);
        ReflectionTestUtils.setField(filter, "requestCount", requestCount);
    }

    @Test
    public void filter_OnePerTime_Passed() throws Exception
    {
        filter.filter(LV, now);
        filter.filter(US, now);
        filter.filter(UK, now);
    }

    @Test
    public void filter_OnePerTimeMultiple_Passed() throws Exception
    {
        filter.filter(US, now);
        filter.filter(UK, now);
        filter.filter(LV, now);
        filter.filter(LV, now + interval);
    }

    @Test(expected = TooManyRequestsException.class)
    public void filter_TwoPerTime_ExceptionThrown() throws Exception
    {
        filter.filter(US, now);
        filter.filter(UK, now);
        filter.filter(LV, now);
        filter.filter(LV, now + interval - 1);
    }
}