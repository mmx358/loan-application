package loan_service.service.validator;

import loan_service.dto.LoanSubmissionDto;
import loan_service.exception.validation.InvalidLoanSubmissionException;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import static java.time.LocalDate.now;
import static loan_service.factory.LoanApplicationDtoFactoryForTests.getInstance;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.junit.Assert.*;

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
public class LoanSubmissionDtoValidatorTest
{
    private final LoanSubmissionDtoValidator validator = new LoanSubmissionDtoValidator();

    private LoanSubmissionDto loanSubmissionDto;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Before
    public void setUp() throws Exception
    {
        loanSubmissionDto = getInstance();
        loanSubmissionDto.setTerm(now().plusMonths(24));
    }

    @Test
    public void validate_AllValid_ExceptionNotThrown() throws Exception
    {
        validator.validate(loanSubmissionDto);
    }

    @Test
    public void validate_AllNull_ExceptionThrown() throws Exception
    {
        LoanSubmissionDto loanSubmissionDtoWithAllNulls = new LoanSubmissionDto();

        boolean wasThrown = false;

        try {
            validator.validate(loanSubmissionDtoWithAllNulls);
        } catch (InvalidLoanSubmissionException e) {
            wasThrown = true;
            assertThat(e.getMessages(), containsInAnyOrder(
                    "loan.amount.invalid",
                    "loan.term.invalid",
                    "loan.name.invalid",
                    "loan.surname.invalid",
                    "loan.personalId.invalid",
                    "loan.countryCode.invalid"));
        }

        assertTrue(wasThrown);
    }

    @Test
    public void validate_AllPossibleNegative_ExceptionThrown() throws Exception
    {
        loanSubmissionDto.setAmount(-1.00);
        loanSubmissionDto.setTerm(now().minusDays(1));
        loanSubmissionDto.setCountryCode("");

        try {
            validator.validate(loanSubmissionDto);
            fail();
        } catch (InvalidLoanSubmissionException e) {
            assertThat(e.getMessages(), containsInAnyOrder(
                    "loan.amount.invalid",
                    "loan.term.invalid",
                    "loan.countryCode.invalid"));
        }
    }

    @Test
    public void validate_AllPossibleTooHigh_ExceptionThrown() throws Exception
    {
        loanSubmissionDto.setAmount(10_000.01);
        loanSubmissionDto.setTerm(now().plusMonths(25));
        loanSubmissionDto.setCountryCode("1234");

        try {
            validator.validate(loanSubmissionDto);
            fail();
        } catch (InvalidLoanSubmissionException e) {
            assertThat(e.getMessages(), containsInAnyOrder(
                    "loan.amount.invalid",
                    "loan.term.invalid",
                    "loan.countryCode.invalid"));
        }
    }

    @Test
    public void validate_NameAndSurnameContainDigits_ExceptionThrown() throws Exception
    {
        loanSubmissionDto.setName("Steven45");
        loanSubmissionDto.setSurname("Briggs3");

        try {
            validator.validate(loanSubmissionDto);
            fail();
        } catch (InvalidLoanSubmissionException e) {
            assertThat(e.getMessages(), containsInAnyOrder(
                    "loan.name.invalid",
                    "loan.surname.invalid"));
        }
    }
}