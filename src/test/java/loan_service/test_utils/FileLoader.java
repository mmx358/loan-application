package loan_service.test_utils;

import org.unitils.thirdparty.org.apache.commons.io.IOUtils;

import java.io.IOException;

public class FileLoader
{
    public String getFileAsString(String path)
    {
        String result = "";

        ClassLoader classLoader = getClass().getClassLoader();
        try {
            result = IOUtils.toString(classLoader.getResourceAsStream(path));
        } catch (IOException e) {
            e.printStackTrace();
        }

        return result;
    }
}
