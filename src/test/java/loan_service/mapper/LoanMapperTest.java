package loan_service.mapper;

import loan_service.dto.LoanDto;
import loan_service.factory.LoanFactoryForTests;
import loan_service.model.entity.Loan;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
public class LoanMapperTest
{
    private final LoanMapper mapper = new LoanMapper();

    @Test
    public void mapLoanToLoanDto_UserIsNull_NullMapped() throws Exception
    {
        Loan loan = LoanFactoryForTests.getInstance(null);
        LoanDto loanDto = mapper.mapLoanToLoanDto(loan);

        assertEquals(loan.getId(), loanDto.getId());
        assertEquals(loan.getAmount(), loanDto.getAmount());
        assertEquals(loan.getTerm(), loanDto.getTerm());
        assertEquals(loan.getCountryCode(), loanDto.getCountryCode());
        assertNull(loanDto.getUser());
        assertNull(loan.getUser());
    }

    @Test
    public void mapLoanToLoanDto_UserNotNull_ObjectsMapped() throws Exception
    {
        Loan loan = LoanFactoryForTests.getInstance();
        LoanDto loanDto = mapper.mapLoanToLoanDto(loan);

        assertEquals(loan.getId(), loanDto.getId());
        assertEquals(loan.getAmount(), loanDto.getAmount());
        assertEquals(loan.getTerm(), loanDto.getTerm());
        assertEquals(loan.getCountryCode(), loanDto.getCountryCode());
        assertEquals(loan.getUser().getId(), loanDto.getUser().getId());
        assertEquals(loan.getUser().getName(), loanDto.getUser().getName());
        assertEquals(loan.getUser().getSurname(), loanDto.getUser().getSurname());
        assertEquals(loan.getUser().getPersonalId(), loanDto.getUser().getPersonalId());
    }
}