package loan_service.mapper;

import loan_service.dto.LoanDto;
import loan_service.dto.LoanSubmissionDto;
import loan_service.dto.UserDto;
import loan_service.model.entity.Loan;
import loan_service.model.entity.User;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import static loan_service.factory.LoanApplicationDtoFactoryForTests.getInstance;
import static org.unitils.reflectionassert.ReflectionAssert.assertReflectionEquals;

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
public class LoanSubmissionMapperTest
{
    private final LoanSubmissionMapper mapper = new LoanSubmissionMapper();
    private LoanSubmissionDto loanSubmissionDto;

    @Before
    public void setUp() throws Exception
    {
        loanSubmissionDto = getInstance();
    }

    @Test
    public void mapLoanApplicationDto_ToLoan_ObjectMapped() throws Exception
    {
        Loan expectedLoan = new Loan(loanSubmissionDto.getAmount(),
                                     loanSubmissionDto.getTerm(),
                                     loanSubmissionDto.getCountryCode());

        Loan actualLoan = mapper.mapLoanSubmissionDtoToLoan(loanSubmissionDto);

        assertReflectionEquals(expectedLoan, actualLoan);
    }

    @Test
    public void mapLoanApplicationDto_ToUser_ObjectMapped() throws Exception
    {
        User expectedUser = new User(loanSubmissionDto.getName(),
                                     loanSubmissionDto.getSurname(),
                                     loanSubmissionDto.getPersonalId());

        User actualUser = mapper.mapLoanApplicationDtoToUser(loanSubmissionDto);

        assertReflectionEquals(expectedUser, actualUser);
    }

    @Test
    public void mapLoanApplicationDto_ToLoanDto_ObjectMapped() throws Exception
    {
        LoanDto expectedLoanDto = new LoanDto(null,
                                              loanSubmissionDto.getAmount(),
                                              loanSubmissionDto.getTerm(),
                                              loanSubmissionDto.getCountryCode(),
                                              new UserDto(null,
                                                          loanSubmissionDto.getName(),
                                                          loanSubmissionDto.getSurname(),
                                                          loanSubmissionDto.getPersonalId()));

        LoanDto actualLoanDto = mapper.mapLoanApplicationDtoToLoanDto(loanSubmissionDto);

        assertReflectionEquals(expectedLoanDto, actualLoanDto);
    }
}
