package loan_service.mapper;

import loan_service.dto.UserDto;
import loan_service.factory.UserFactoryForTests;
import loan_service.model.entity.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
public class UserMapperTest
{
    private final UserMapper mapper = new UserMapper();

    @Test
    public void mapUser_ToUserDto() throws Exception
    {
        User user = UserFactoryForTests.getInstance();

        UserDto userDto = mapper.mapUserToUserDto(user);

        assertEquals(user.getId(), userDto.getId());
        assertEquals(user.getName(), userDto.getName());
        assertEquals(user.getSurname(), userDto.getSurname());
        assertEquals(user.getPersonalId(), userDto.getPersonalId());
    }
}