package loan_service.factory;

import loan_service.dto.LoanSubmissionDto;

import java.time.LocalDate;

public class LoanApplicationDtoFactoryForTests
{
    public static LoanSubmissionDto getInstance()
    {
        return new LoanSubmissionDto(
                333.33,
                LocalDate.of(2017, 10, 9),
                "Rick",
                "Strange",
                "GHT-8093",
                "lv");
    }
}
