package loan_service.factory;

import loan_service.model.entity.Loan;
import loan_service.model.entity.User;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

public class LoanFactoryForTests
{
    public static Loan getInstance()
    {
        return new Loan(
                15L,
                450.50,
                LocalDate.of(2017, 10, 19),
                "lv",
                UserFactoryForTests.getInstance());
    }

    public static Loan getInstance(User user)
    {
        Loan loan = getInstance();
        loan.setUser(user);
        return loan;
    }

    public static List<Loan> getListOfThree()
    {
        List<Loan> loans = new LinkedList<>();
        loans.add(new Loan(687.43, LocalDate.of(2017, 9, 12), "lv"));
        loans.add(new Loan(1340.00, LocalDate.of(2018, 2, 19), "lv"));
        loans.add(new Loan(500.79, LocalDate.of(2017, 3, 28), "lv"));
        return loans;
    }
}
