package loan_service.factory;

import loan_service.model.entity.Loan;
import loan_service.model.entity.User;

import java.util.LinkedList;
import java.util.List;

public class UserFactoryForTests
{
    public static User getInstance()
    {
        return new User(
                15L,
                "Ted",
                "Flockhart",
                "TDFK-92836",
                new LinkedList<>());
    }

    public static User getInstance(List<Loan> loans)
    {
        User user = getInstance();
        user.setLoans(loans);
        return user;
    }

    public static List<User> getListOfThree()
    {
        List<User> users = new LinkedList<>();
        users.add(new User("John", "Doe", "210389-45863"));
        users.add(new User("Eddie", "Stokes", "170991-93745"));
        users.add(new User("Richard", "Adams", "091286-09536"));
        return users;
    }
}
