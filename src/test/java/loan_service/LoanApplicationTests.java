package loan_service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import loan_service.dto.LoanDto;
import loan_service.dto.LoanSubmissionDto;
import loan_service.factory.LoanApplicationDtoFactoryForTests;
import loan_service.factory.LoanFactoryForTests;
import loan_service.factory.UserFactoryForTests;
import loan_service.mapper.LoanMapper;
import loan_service.mapper.LoanSubmissionMapper;
import loan_service.model.entity.Loan;
import loan_service.model.entity.PersonalId;
import loan_service.model.entity.User;
import loan_service.model.repository.LoanRepository;
import loan_service.model.repository.PersonalIdBlacklistRepository;
import loan_service.model.repository.UserRepository;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import java.nio.charset.Charset;
import java.time.LocalDateTime;
import java.util.List;

import static java.lang.Thread.sleep;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;
import static org.unitils.reflectionassert.ReflectionAssert.assertReflectionEquals;

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@SpringBootTest(classes = LoanApplication.class)
@WebAppConfiguration
public class LoanApplicationTests
{
    private final MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
                                                  MediaType.APPLICATION_JSON.getSubtype(),
                                                  Charset.forName("utf8"));

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private LoanRepository loanRepository;

    @Autowired
    private PersonalIdBlacklistRepository blacklistRepository;

    private final ObjectMapper objectMapper = new ObjectMapper();
    private final TypeReference<List<LoanDto>> loanDtoListType = new TypeReference<List<LoanDto>>()
    {
    };

    @Value("${request.rate.interval}")
    private Long interval;

    @Value("${request.rate.count}")
    private Long requestCount;

    private List<User> users;
    private List<Loan> loans;

    @Before
    public void setup() throws Exception
    {
        this.mockMvc = webAppContextSetup(webApplicationContext).build();

        users = UserFactoryForTests.getListOfThree();
        users.forEach(user -> user.setId(null));
        userRepository.save(users);

        loans = LoanFactoryForTests.getListOfThree();
        loans.forEach(loan -> loan.setId(null));
        loans.get(0).setUser(users.get(0));
        loans.get(1).setUser(users.get(0));
        loans.get(2).setUser(users.get(1));

        loanRepository.save(loans);
    }

    @After
    public void tearDown() throws Exception
    {
        clearDataBase();
    }

    private void clearDataBase()
    {
        List<Loan> allLoans = loanRepository.findAll();
        allLoans
                .stream()
                .map(Loan::getId)
                .forEach(loanRepository::delete);

        List<User> allUsers = userRepository.findAll();
        allUsers
                .stream()
                .map(User::getId)
                .forEach(userRepository::delete);

        List<PersonalId> allBlacklistedPersonalIds = blacklistRepository.findAll();
        allBlacklistedPersonalIds
                .stream()
                .map(PersonalId::getId)
                .forEach(blacklistRepository::delete);
    }

    @Test
    public void getAllLoans_LoansNotFound_EmptyListReturned() throws Exception
    {
        clearDataBase();

        mockMvc
                .perform(get("/loan/")
                                 .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(0)));
    }

    @Test
    public void getAllLoans_LoansFromTwoUsersAreFoundAndOneUserWithoutLoans_LoanListReturned() throws Exception
    {
        List<LoanDto> expectedLoanDtoList = new LoanMapper().mapLoanListToLoanDtoList(loans);

        String jsonContent = mockMvc
                .perform(get("/loan/")
                                 .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andReturn().getResponse().getContentAsString();

        List<LoanDto> returnedLoans = objectMapper.readValue(jsonContent, loanDtoListType);
        assertThat(returnedLoans, containsInAnyOrder(expectedLoanDtoList.toArray()));
    }

    @Test
    public void getAllLoans_ByUserAndPersonalIdIsBlank_EmptyListReturned() throws Exception
    {
        String errorMessage = mockMvc
                .perform(get("/loan/")
                                 .param("personalId", "")
                                 .contentType(contentType))
                .andExpect(status().isUnprocessableEntity())
                .andReturn().getResponse().getErrorMessage();

        assertEquals("user.filter.invalid", errorMessage);
    }

    @Test
    public void getAllLoans_ByUserWhichNotExists_EmptyListReturned() throws Exception
    {
        String jsonContent = mockMvc
                .perform(get("/loan/")
                                 .param("personalId", "Assuming_this_code_not_in_DB")
                                 .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andReturn().getResponse().getContentAsString();

        List<LoanDto> returnedLoans = objectMapper.readValue(jsonContent, loanDtoListType);
        assertEquals(0, returnedLoans.size());
    }

    @Test
    public void getAllLoans_ByUserWithoutLoans_EmptyListReturned() throws Exception
    {
        String jsonContent = mockMvc
                .perform(get("/loan/")
                                 .param("personalId", users.get(2).getPersonalId())
                                 .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andReturn().getResponse().getContentAsString();

        List<LoanDto> returnedLoans = objectMapper.readValue(jsonContent, loanDtoListType);
        assertEquals(0, returnedLoans.size());
    }

    @Test
    public void saveLoan_UsersNotExists_LoanSaved() throws Exception
    {
        sleep(interval);

        LoanSubmissionDto loanSubmissionDto = LoanApplicationDtoFactoryForTests.getInstance();

        LoanDto expectedLoanDto = new LoanSubmissionMapper().mapLoanApplicationDtoToLoanDto(loanSubmissionDto);

        String jsonContentForSavedLoan = mockMvc
                .perform(post("/loan/")
                                 .content(new ObjectMapper().writeValueAsString(loanSubmissionDto))
                                 .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andReturn().getResponse().getContentAsString();

        LoanDto actualReturnedLoan = objectMapper.readValue(jsonContentForSavedLoan, LoanDto.class);

        assertNotNull(actualReturnedLoan.getId());
        assertNotNull(actualReturnedLoan.getUser());
        assertNotNull(actualReturnedLoan.getUser().getId());

        expectedLoanDto.setId(actualReturnedLoan.getId());
        expectedLoanDto.getUser().setId(actualReturnedLoan.getUser().getId());

        assertReflectionEquals(expectedLoanDto, actualReturnedLoan);

        String jsonContentForReturnedLoans = mockMvc
                .perform(get("/loan/")
                                 .param("personalId", expectedLoanDto.getUser().getPersonalId())
                                 .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andReturn().getResponse().getContentAsString();

        List<LoanDto> returnedLoans = objectMapper.readValue(jsonContentForReturnedLoans, loanDtoListType);
        assertThat(returnedLoans, containsInAnyOrder(expectedLoanDto));
    }

    @Test
    public void saveLoan_UsersExists_LoanSaved() throws Exception
    {
        sleep(interval);

        User user = UserFactoryForTests.getInstance();
        user.setId(null);
        userRepository.save(user);

        LoanSubmissionDto loanSubmissionDto = LoanApplicationDtoFactoryForTests.getInstance();
        loanSubmissionDto.setName(user.getName());
        loanSubmissionDto.setSurname(user.getSurname());
        loanSubmissionDto.setPersonalId(user.getPersonalId());

        LoanDto expectedLoanDto = new LoanSubmissionMapper().mapLoanApplicationDtoToLoanDto(loanSubmissionDto);

        String jsonContentForSavedLoan = mockMvc
                .perform(post("/loan/")
                                 .content(new ObjectMapper().writeValueAsString(loanSubmissionDto))
                                 .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andReturn().getResponse().getContentAsString();

        LoanDto actualReturnedLoan = objectMapper.readValue(jsonContentForSavedLoan, LoanDto.class);

        assertNotNull(actualReturnedLoan.getId());
        assertNotNull(actualReturnedLoan.getUser());
        assertNotNull(actualReturnedLoan.getUser().getId());

        expectedLoanDto.setId(actualReturnedLoan.getId());
        expectedLoanDto.getUser().setId(user.getId());

        assertReflectionEquals(expectedLoanDto, actualReturnedLoan);

        String jsonContentForReturnedLoans = mockMvc
                .perform(get("/loan/")
                                 .param("personalId", expectedLoanDto.getUser().getPersonalId())
                                 .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andReturn().getResponse().getContentAsString();

        List<LoanDto> returnedLoans = objectMapper.readValue(jsonContentForReturnedLoans, loanDtoListType);
        assertThat(returnedLoans, containsInAnyOrder(expectedLoanDto));
    }

    @Test
    public void saveLoan_UsersExistsAndUserDataMismatch_409Responded() throws Exception
    {
        sleep(interval);

        User user = UserFactoryForTests.getInstance();
        user.setId(null);
        userRepository.save(user);

        LoanSubmissionDto loanSubmissionDto = LoanApplicationDtoFactoryForTests.getInstance();
        loanSubmissionDto.setName(user.getName() + "_");
        loanSubmissionDto.setSurname(user.getSurname() + "_");
        loanSubmissionDto.setPersonalId(user.getPersonalId());

        String errorMessage = mockMvc
                .perform(post("/loan/")
                                 .content(new ObjectMapper().writeValueAsString(loanSubmissionDto))
                                 .contentType(contentType))
                .andExpect(status().isConflict())
                .andReturn().getResponse().getErrorMessage();

        assertEquals("user.data.mismatch", errorMessage);
    }

    @Test
    public void saveLoan_PersonalIdBlacklisted_403Responded() throws Exception
    {
        sleep(interval);

        PersonalId personalId = new PersonalId("whatever-id");
        blacklistRepository.save(personalId);

        LoanSubmissionDto loanSubmissionDto = LoanApplicationDtoFactoryForTests.getInstance();
        loanSubmissionDto.setPersonalId(personalId.getId());

        String errorMessage = mockMvc
                .perform(post("/loan/")
                                 .content(new ObjectMapper().writeValueAsString(loanSubmissionDto))
                                 .contentType(contentType))
                .andExpect(status().isForbidden())
                .andReturn().getResponse().getErrorMessage();

        assertEquals("personalId.in.blacklist", errorMessage);
    }

    @Test
    public void saveLoan_AmountIsNegative_422Responded() throws Exception
    {
        sleep(interval);

        LoanSubmissionDto loanSubmissionDto = LoanApplicationDtoFactoryForTests.getInstance();
        loanSubmissionDto.setAmount(-1.00);

        String errorMessage = mockMvc
                .perform(post("/loan/")
                                 .content(new ObjectMapper().writeValueAsString(loanSubmissionDto))
                                 .contentType(contentType))
                .andExpect(status().isUnprocessableEntity())
                .andReturn().getResponse().getErrorMessage();

        assertEquals("loan.submission.invalid", errorMessage);
    }

    @Test
    public void saveLoan_TwoFromSameCountryPerTimeInterval_429Responded() throws Exception
    {
        sleep(interval);

        LoanSubmissionDto firstLoanSubmissionDto = LoanApplicationDtoFactoryForTests.getInstance();
        LoanSubmissionDto secondLoanSubmissionDto = LoanApplicationDtoFactoryForTests.getInstance();

        System.out.println(LocalDateTime.now());

        mockMvc
                .perform(post("/loan/")
                                 .content(new ObjectMapper().writeValueAsString(firstLoanSubmissionDto))
                                 .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType));

        System.out.println(LocalDateTime.now());

        String errorMessage = mockMvc
                .perform(post("/loan/")
                                 .content(new ObjectMapper().writeValueAsString(secondLoanSubmissionDto))
                                 .contentType(contentType))
                .andExpect(status().isTooManyRequests())
                .andReturn().getResponse().getErrorMessage();

        System.out.println(LocalDateTime.now());

        assertEquals("too.many.requests.from.same.country", errorMessage);
    }
}
